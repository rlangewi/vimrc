" Ryan Langewisch's .vimrc

" We don't care about vi compatibility
set nocompatible              

" Necessary for Vundle to work
filetype off

" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Vundle Plugins
Plugin 'VundleVim/Vundle.vim'
Plugin 'nanotech/jellybeans.vim'
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'rking/ag.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'BufOnly.vim'
Plugin 'tpope/vim-commentary'
Plugin 'moll/vim-bbye'
Plugin 'kchmck/vim-coffee-script'
Plugin 'tpope/vim-sleuth'
Plugin 'tpope/vim-markdown'
Plugin 'leafgarland/typescript-vim'

call vundle#end()

" Turn file recognition back on
filetype plugin indent on

" Line numbers
set number

" Tab settings
set tabstop=4
set shiftwidth=4
set autoindent
set expandtab
set list
set listchars=tab:>-

" Turn off autocommenting (Annoys me more often than not).
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Theme
syntax enable
set laststatus=2
colorscheme jellybeans

" Use search highlighting
set hlsearch

" Airline
let g:airline_theme='bubblegum'
let g:airline#extensions#tabline#enabled = 1
let g:airline_left_sep=''
let g:airline_right_sep=''
let g:airline_powerline_fonts = 1
let g:airline#extensions#whitespace#enabled = 0

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Don't show tab type
let g:airline#extensions#tabline#show_tab_type = 0

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" Default ctrlp to search by filename with regex
let g:ctrlp_by_filename = 1
let g:ctrlp_regexp = 1

" Automatically highlight when using silver searcher
let g:ag_highlight = 1

" Use ackgrep if silver searcher isn't available
set grepprg=ack-grep

" Ignore files in .gitignore if in git repo
let g:ctrlp_user_command = {
  \ 'types': {1: ['.git', 'cd %s && git ls-files --cached --exclude-standard --others']},
  \ 'fallback': 'find %s -type f',
  \ 'ignore': 1
  \ }

" Ignore files we wouldn't edit often
set wildignore+=*.meta
set wildignore+=*.wav
set wildignore+=*.prefab
set wildignore+=*.asset
set wildignore+=*.unity
set wildignore+=*.gitignore
set wildignore+=*.swp

" Better Java highlighting
let java_highlight_all = 1
let java_highlight_functions = "style"
let java_allow_cpp_keywords = 1

" Improve language commenting with commentary
autocmd FileType c,cpp,cs,java setlocal commentstring=//\ %s

" Automatically open search results in window after using grep
augroup myvimrc
  autocmd!
  autocmd QuickFixCmdPost [^l]* cwindow
  autocmd QuickFixCmdPost l*    lwindow
augroup END

" Map leader to space
let mapleader = " "

" Always center when moving through search results
nnoremap n nzz
nnoremap N Nzz

" Custom commands
nnoremap <leader>a :Ag! 
nnoremap <leader>A :Ag! -Q "
nnoremap <leader>b :BufOnly<CR>:AirlineRefresh<CR>
nnoremap <leader>c :let @/ = ""<CR>
nnoremap <leader>d "_d
nnoremap <leader>E :E<CR>
nnoremap <leader>e :e.<CR>
nnoremap <leader>g :grep! -R ''<left>
nnoremap <leader>G :Gblame<CR>
nnoremap <leader>N :bp<CR>
nnoremap <leader>n :bn<CR>
nnoremap <leader>O m`O<ESC>``
nnoremap <leader>o m`o<ESC>``
nnoremap <leader>p "0p
nnoremap <leader>P "0P
nnoremap <leader>q :q<CR>
nnoremap <leader>r :%s///g<left><left>
nnoremap <leader>s :shell<CR>
nnoremap <leader>t :set wrap! wrap?<CR>
nnoremap <leader>w :w<CR>
nnoremap <leader>x :Bdelete<CR>
nnoremap <leader>X :Bdelete<CR>:bn<CR>
nnoremap <leader>% :so %<CR>
nnoremap <leader>[ :CtrlPClearCache<CR>

vnoremap <leader>d "_d
vnoremap <space>/ y/\V<C-R>"<CR>
