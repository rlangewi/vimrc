# README #

In order to keep the .vimrc file under version control, you can use a symlink. This can be done by cloning this repo into the home directory, and then running the following command:


```
#!bash

ln -s ~/vimrc/.vimrc ~/.vimrc
```

Note that if a .vimrc file already exists, it will need to be removed before setting up the symlink:


```
#!bash

rm ~/.vimrc
```

In order for all of the plugins to load, you need to clone the Vundle repo, found here: https://github.com/VundleVim/Vundle.vim.

Once Vundle is installed, you will want to run the ```:PluginInstall``` command in vim so Vundle can do its thing.

Also, it is recommended to install the [Inconsolata-g for Powerline](https://github.com/powerline/fonts/tree/master/Inconsolata-g) font and set it as the terminal font, in my case, the font for iTerm2. This will allow special characters such as the git branch symbol to be supported.

For silver searcher (:Ag) to work, you will need to install the external tool: https://github.com/ggreer/the_silver_searcher.